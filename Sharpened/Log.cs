namespace Sharpened;

public static class Log
{
    public enum Type
    {
        Info,
        Warning,
        Error
    }

    public static void Info(object message)
    {
        LogInternal(Type.Info, message);
    }

    public static void Warning(object message)
    {
        LogInternal(Type.Warning, message);
    }

    public static void Error(object message)
    {
        LogInternal(Type.Error, message);
    }

    public static void Force(Type logType, object message)
    {
        LogInternal(logType, message);
    }

    private static void LogInternal(Type logType, object message)
    {
        TextWriter writer = logType <= Type.Warning ? Console.Out : Console.Error;

        switch (logType)
        {
            case Type.Info:
                writer.WriteLine();
                break;
            case Type.Warning:
                writer.WriteLine("[Warning] ");
                break;
            case Type.Error:
                writer.WriteLine("[Error] ");
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(logType), logType, null);
        }

        writer.Write(message);
        writer.Flush();
    }
}
