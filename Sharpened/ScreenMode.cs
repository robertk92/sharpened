namespace Sharpened;

public enum ScreenMode
{
    Fullscreen,
    BorderlessFullscreen,
    Windowed
}
