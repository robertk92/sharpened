﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Sharpened;

public struct ProfilerScope : IDisposable
{
    public ProfilerScope(string label) {
        Profiler.BeginSample(label);
    }

    public void Dispose() {
        Profiler.EndSample();
    }
}

public static class Profiler
{
    [Conditional("PROFILER")]
    public static void BeginSample(string label) {

    }

    [Conditional("PROFILER")]
    public static void EndSample() {

    }
}