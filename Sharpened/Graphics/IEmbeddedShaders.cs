namespace Sharpened.Graphics;

internal interface IEmbeddedShaders
{
    ShaderFormat ShaderFormat { get; }
    byte[] FullscreenVert { get; }
    byte[] TextMsdfFrag { get; }
    byte[] TextTransformVert { get; }
    byte[] VideoYuv2RgbaFrag { get; }
}
