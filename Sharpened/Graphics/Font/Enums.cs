namespace Sharpened.Graphics
{
    public enum HorizontalAlignment
    {
        Left,
        Center,
        Right
    }

    public enum VerticalAlignment
    {
        Baseline,
        Top,
        Middle,
        Bottom
    }
}
