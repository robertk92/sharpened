﻿namespace Sharpened.Audio
{
	public enum SoundState
	{
		Playing,
		Paused,
		Stopped
	}
}
