namespace Sharpened.Audio
{
	public enum FilterType
	{
		None,
		LowPass,
		BandPass,
		HighPass
	}
}
