﻿using System.Numerics;

namespace Sharpened;

public struct MouseState
{
    public Vector2 position;
    public ButtonState left;
    public ButtonState middle;
    public ButtonState right;
    public ButtonState x1;
    public ButtonState x2;
    public int wheel;
}