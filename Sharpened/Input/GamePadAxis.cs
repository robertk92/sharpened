﻿namespace Sharpened;

public enum GamePadAxis
{
    LeftTrigger,
    RightTrigger,
    LeftThumbStickX,
    LeftThumbStickY,
    RightThumbStickX,
    RightThumbStickY
}