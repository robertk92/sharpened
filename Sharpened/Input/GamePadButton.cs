﻿namespace Sharpened;

public enum GamePadButton
{
    Start,
    A,
    B,
    X,
    Y,
    DPadLeft,
    DPadRight,
    DPadUp,
    DPadDown,
    RightShoulder,
    LeftShoulder,
    LeftStick,
    RightStick,
    Back,
    BigButton
}