﻿namespace Sharpened;

public enum ButtonState
{
    Released,
    Pressed
}