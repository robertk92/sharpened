namespace Sharpened.Video
{
	public enum VideoState
	{
		Playing,
		Paused,
		Stopped
	}
}
