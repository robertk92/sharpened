namespace Sharpened;

public static class MathEx
{
    public static float ToNearestMultiple(float value, float multiple) {
        return MathF.Round(value / multiple, MidpointRounding.AwayFromZero) * multiple;
    }

    public static float Normalize(float value, float oldMin, float oldMax) {
        return Remap(value, oldMin, oldMax, 0f, 1f);
    }

    public static float Denormalize(float value, float newMin, float newMax) {
        return Remap(value, 0f, 1f, newMin, newMax);
    }

    public static float Remap(float value, float oldMin, float oldMax, float newMin, float newMax) {
        return (value - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
    }
}
