﻿using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace Sharpened;

public class AssertionException(string msg) : Exception(msg);

public static class Assert
{
    [Conditional("DEBUG"), DebuggerHidden]
    public static void Debug([DoesNotReturnIf(false)] bool cond, [CallerArgumentExpression("cond")] string expression = "") {
        if (!cond) {
            throw new AssertionException(expression);
        }
    }

    [DebuggerHidden]
    public static void Always([DoesNotReturnIf(false)] bool cond, [CallerArgumentExpression("cond")] string expression = "") {
        if (!cond) {
            throw new AssertionException(expression);
        }
    }
}