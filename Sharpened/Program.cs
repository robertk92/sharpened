﻿namespace Sharpened;

internal class Program
{
    private static void Main(string[] args)
    {
        Engine.Run(args);
    }
}
